<?php

namespace App\Provider;

use App\Entity\Bookmark;

interface BookmarkProviderInterface
{
    /**
     * Create a bookmark using oEmbed
     *
     * @param string $url
     * @return Bookmark
     */
    public function createBookmark(string $url): Bookmark;

    /**
     * Return domain
     *
     * @return string
     */
    public function getDomain(): string;
}

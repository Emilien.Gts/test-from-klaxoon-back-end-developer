<?php

namespace App\Provider;

use App\Entity\Bookmark;
use App\Entity\Bookmark\BookmarkImage;
use Embed\Embed;

class FlickrBookmarkProvider implements BookmarkProviderInterface
{
    public const DOMAIN = 'www.flickr.com';

    /**
     * Create a bookmark using oEmbed
     *
     * @param string $url
     * @return Bookmark
     */
    public function createBookmark(string $url): Bookmark
    {
        $embed = new Embed();
        $bookmark = new BookmarkImage();

        $info = $embed->get($url);
        $data = $info->getOEmbed();

        $bookmark->setURL($url)
            ->setProviderName($data->get('provider_name'))
            ->setTitle($data->get('title'))
            ->setAuthor($data->get('author_name'))
            ->setWidth($data->get('width'))
            ->setHeight($data->get('height'));

        return $bookmark;
    }

    /**
     * Return domain
     *
     * @return string
     */
    public function getDomain(): string
    {
        return self::DOMAIN;
    }
}

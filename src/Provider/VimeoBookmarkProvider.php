<?php

namespace App\Provider;

use Embed\Embed;
use App\Entity\Bookmark;
use App\Entity\Bookmark\BookmarkVideo;

class VimeoBookmarkProvider implements BookmarkProviderInterface
{
    public const DOMAIN = 'vimeo.com';

    /**
     * Create a bookmark using oEmbed
     *
     * @param string $url
     * @return Bookmark
     */
    public function createBookmark(string $url): Bookmark
    {
        $embed = new Embed();
        $bookmark = new BookmarkVideo();

        $info = $embed->get($url);
        $data = $info->getOEmbed();

        $bookmark->setURL($url)
            ->setProviderName($data->get('provider_name'))
            ->setTitle($data->get('title'))
            ->setAuthor($data->get('author_name'))
            ->setWidth($data->get('width'))
            ->setHeight($data->get('height'))
            ->setDuration($info->getOEmbed()->get('duration'))
            ->setPublishedAt(
                \DateTimeImmutable::createFromFormat(
                    'Y-m-d H:i:s',
                    $info->getOEmbed()->get('upload_date')
                )
            );

        return $bookmark;
    }

    /**
     * Return domain
     *
     * @return string
     */
    public function getDomain(): string
    {
        return self::DOMAIN;
    }
}

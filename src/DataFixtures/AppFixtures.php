<?php

namespace App\DataFixtures;

use App\Entity\Bookmark\BookmarkImage;
use App\Entity\Bookmark\BookmarkVideo;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // Setting up bookmark video
        $bookmarkImage = new BookmarkImage();
        $bookmarkImage->setURL('https://vimeo.com/311692367')
            ->setProviderName('Vimeo')
            ->setTitle('ESPN+ UFC')
            ->setAuthor('Sung I. Sohn')
            ->setWidth(360.0)
            ->setHeight(640.0);

        $manager->persist($bookmarkImage);

        // Setting up bookmark image
        $bookmarkVideo = new BookmarkVideo();
        $bookmarkVideo->setURL('https://www.flickr.com/photos/manolinlao/51415625566/in/photolist-2mkqwbS-72ePvn-72iNzf-bCDLqu-8f5Sdj-tNVQWe-c45Hrb-WUWitk-fm9nxm-wPb5Y-7ocvoR-N2ww9z-Na8Hda-bhf18M-7Uuh93-24G4Pej-N2wvWF-Na5CNV-7EDwz4-McqnN9-3VJDq9-McsAur-N2BXbt-N2yS1e-Mc6Hvc-N2BXfB-4dQHRR-8AhkRz-N7nfcn-89vqxx-7eZjuJ-7eVq6x-7C9cJw-Mcqnyw-vjRZ6-c7BfAU-Naa6gY-ML2Y4h-dfA513-5Cm79k-N7jwCK-N6JCtL-wQkLS7-N2BXna-6fvSHN-dr3TmJ-MGZ6Qy-N2g6Qe-Mc6H9k-MhasxZ')
            ->setProviderName('Flickr')
            ->setTitle('Developer')
            ->setAuthor('Manolin Lao')
            ->setWidth(768.0)
            ->setHeight(1024.0)
            ->setDuration(35);

        $manager->persist($bookmarkVideo);

        $manager->flush();
    }
}

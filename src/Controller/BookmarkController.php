<?php

namespace App\Controller;

use App\Entity\Bookmark;
use App\Service\BookmarkService;
use App\Repository\BookmarkRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/api/bookmarks")
 */
class BookmarkController extends AbstractController
{
    /**
     * Get all bookmarks
     * 
     * @Route("", name="api_bookmarks_get_bookmarks", methods={"GET"})
     *
     * @param BookmarkRepository $bookmarkRepository
     * @return Response
     */
    public function getBookmarks(
        BookmarkRepository $bookmarkRepository
    ): Response {
        return $this->json(
            $bookmarkRepository->findAll(),
            200,
            [],
            ['groups' => ['read']]
        );
    }

    /**
     * Create one bookmark
     * 
     * @Route("", name="api_bookmarks_create_bookmark", methods={"POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function create(
        Request $request,
        BookmarkService $bookmarkService,
        EntityManagerInterface $manager
    ): Response {
        $data = json_decode($request->getContent());
        $bookmark = $bookmarkService->createBookmark($data->url);

        $manager->persist($bookmark);
        $manager->flush();

        return $this->json(
            $bookmark,
            Response::HTTP_CREATED,
            [],
            ['groups' => ['read']]
        );
    }

    /**
     * Delete a bookmark
     * 
     * @Route("/{id}", name="api_bookmarks_delete_bookmark", methods={"DELETE"})
     *
     * @param Bookmark $bookmark
     * @return Response
     */
    public function delete(Bookmark $bookmark, EntityManagerInterface $manager): Response
    {
        try {
            $manager->remove($bookmark);
            $manager->flush();

            return new Response('', Response::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            return $this->json([
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $e->getMessage()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

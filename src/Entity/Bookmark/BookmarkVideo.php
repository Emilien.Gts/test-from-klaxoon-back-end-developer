<?php

namespace App\Entity\Bookmark;

use App\Entity\Bookmark;
use App\Repository\Bookmark\BookmarkVideoRepository;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BookmarkVideoRepository::class)
 */
class BookmarkVideo extends Bookmark
{
    /**
     * @ORM\Column(type="float", nullable=true)
     * 
     * @Groups({"read"})
     */
    private ?float $width = null;

    /**
     * @ORM\Column(type="float", nullable=true)
     * 
     * @Groups({"read"})
     */
    private ?float $height = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * 
     * @Groups({"read"})
     */
    private $duration;

    public function getWidth(): ?float
    {
        return $this->width;
    }

    public function setWidth(?float $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getHeight(): ?float
    {
        return $this->height;
    }

    public function setHeight(?float $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }
}

<?php

namespace App\Entity\Bookmark;

use App\Entity\Bookmark;
use App\Repository\Bookmark\BookmarkImageRepository;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidV4Generator;

/**
 * @ORM\Entity(repositoryClass=BookmarkImageRepository::class)
 */
class BookmarkImage extends Bookmark
{
    /**
     * @ORM\Column(type="float", nullable=true)
     * 
     * @Groups({"read"})
     */
    private ?float $width = null;

    /**
     * @ORM\Column(type="float", nullable=true)
     * 
     * @Groups({"read"})
     */
    private ?float $height = null;

    public function getWidth(): ?float
    {
        return $this->width;
    }

    public function setWidth(?float $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getHeight(): ?float
    {
        return $this->height;
    }

    public function setHeight(?float $height): self
    {
        $this->height = $height;

        return $this;
    }
}

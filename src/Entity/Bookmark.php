<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Bookmark\BookmarkImage;
use App\Repository\BookmarkRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Entity\Bookmark\BookmarkVideo;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass=BookmarkRepository::class)
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discriminator", type="string")
 * @ORM\DiscriminatorMap({
 *      "image"=BookmarkImage::class,
 *      "video"=BookmarkVideo::class
 * })
 */
abstract class Bookmark
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="doctrine.uuid_generator")
     * 
     * @Groups({"read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1020)
     * 
     * @Groups({"read"})
     * 
     * @Assert\Url(message="The content is not a valid URL.")
     */
    private string $URL;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     * @Groups({"read"})
     */
    private string $providerName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     * @Groups({"read"})
     */
    private ?string $title = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     * @Groups({"read"})
     */
    private ?string $author = null;

    /**
     * @ORM\Column(type="datetime_immutable")
     * 
     * @Groups({"read"})
     * 
     * @Assert\NotNull(message="The date added cannot be null.")
     */
    private \DateTimeImmutable $addedAt;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     * 
     * @Groups({"read"})
     */
    private ?\DateTimeImmutable $publishedAt = null;

    public function __construct()
    {
        $this->addedAt = new \DateTimeImmutable();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getURL(): ?string
    {
        return $this->URL;
    }

    public function setURL(string $URL): self
    {
        $this->URL = $URL;

        return $this;
    }

    public function getProviderName(): ?string
    {
        return $this->providerName;
    }

    public function setProviderName(string $providerName): self
    {
        $this->providerName = $providerName;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(?string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getAddedAt(): ?\DateTimeImmutable
    {
        return $this->addedAt;
    }

    public function setAddedAt(\DateTimeImmutable $addedAt): self
    {
        $this->addedAt = $addedAt;

        return $this;
    }

    public function getPublishedAt(): ?\DateTimeImmutable
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?\DateTimeImmutable $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }
}

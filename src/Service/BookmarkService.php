<?php

namespace App\Service;

use App\Entity\Bookmark;
use App\Provider\VimeoBookmarkProvider;
use App\Provider\FlickrBookmarkProvider;
use App\Provider\BookmarkProviderInterface;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class BookmarkService
{
    private array $bookmarkProviders = [];

    public function __construct(
        FlickrBookmarkProvider $flickrBookmarkProvider,
        VimeoBookmarkProvider $vimeoBookmarkProvider
    ) {
        $this->flickrBookmarkProvider = $flickrBookmarkProvider;
        $this->vimeoBookmarkProvider = $vimeoBookmarkProvider;

        $this->bookmarkProviders = [
            $flickrBookmarkProvider,
            $vimeoBookmarkProvider,
        ];
    }

    /**
     * Create a bookmark using provider
     *
     * @param string $url
     * @return Bookmark|null
     */
    public function createBookmark(string $url): ?Bookmark
    {
        $provider = $this->findProvider($url);

        if (!$provider) {
            throw new Exception('Can\'t create bookmark with this link.', Response::HTTP_BAD_REQUEST);
        }

        return $provider->createBookmark($url);
    }

    /**
     * Find the appropriate provider based on the DOMAIN
     *
     * @param string $url
     * @return BookmarkProviderInterface|null
     */
    public function findProvider(string $url): ?BookmarkProviderInterface
    {
        $parse = parse_url($url);
        $domain = $parse['host'];

        foreach ($this->bookmarkProviders as $provider) {
            if ($provider->getDomain() === $domain) {
                return $provider;
            }
        }

        return null;
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220417103144 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bookmark (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', url VARCHAR(1020) NOT NULL, provider_name VARCHAR(255) NOT NULL, title VARCHAR(255) DEFAULT NULL, author VARCHAR(255) DEFAULT NULL, added_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', published_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', discriminator VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bookmark_image (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', width DOUBLE PRECISION DEFAULT NULL, height DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bookmark_video (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', width DOUBLE PRECISION DEFAULT NULL, height DOUBLE PRECISION DEFAULT NULL, duration INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bookmark_image ADD CONSTRAINT FK_4ABA0298BF396750 FOREIGN KEY (id) REFERENCES bookmark (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE bookmark_video ADD CONSTRAINT FK_F340DCEBBF396750 FOREIGN KEY (id) REFERENCES bookmark (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bookmark_image DROP FOREIGN KEY FK_4ABA0298BF396750');
        $this->addSql('ALTER TABLE bookmark_video DROP FOREIGN KEY FK_F340DCEBBF396750');
        $this->addSql('DROP TABLE bookmark');
        $this->addSql('DROP TABLE bookmark_image');
        $this->addSql('DROP TABLE bookmark_video');
    }
}
